package com.example.android.jamey.footballpicks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import java.text.NumberFormat;

/**
 * This is a program to calculate scores based on whether nick picks winning or loseing
 * teams for any given sunday.
 * In the app, he enters the team that the line picks, then he makes his own pick.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    int[] scoreValue = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //Until teams win or lose all score values are zero.

    /**
     * This method is called when the submit button is clicked.
     */
    public void submitPicks(View view) {
        display();
        displayTotal();
    }

    /**
     * This method displays the given game score value on the screen by iterating through the
     * games by rank
     */
    private void display() {
        for (int i = 0; i < 16; ++i) {
            String idName = ("game" + i + "Score"); //Using i to create a gameid variable
            int id = getResources().getIdentifier(idName, "id", getPackageName()); //using the idName to get id information as stored by android
            TextView scoreTextView = (TextView) findViewById(id);
            scoreTextView.setText("" + scoreValue[i]); //Score value is determined below
        }
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        for (int i =0; i < 16; ++i) {
            String idNameWin = ("game" + i + "Win"); //Using i to create a game id for winning condition
            String idNameLose = ("game" + i + "Lose"); // and losing condition
            int idWin = getResources().getIdentifier(idNameWin, "id", getPackageName());
            int idLose = getResources().getIdentifier(idNameLose, "id", getPackageName());
            // Check which radio button was clicked
            if (view.getId() == idWin) {
                if (checked) {
                    scoreValue[i] = (i + 1); //if nick's team wins, a positive score is put into the array
                }
            }
            if (view.getId() == idLose) {
                if (checked) {
                    scoreValue[i] = -(i + 1); //if nick's team loses, a negative score is put into the array
                }
            }
        }
    }
    /**
     * This method displays the total score on the screen.
     */
    private void displayTotal() {
        int totalScore = 0;
        for (int i=0; i<16; ++i){
            totalScore += scoreValue[i]; //The total score tallies all the winning and losing scores
        }
        TextView priceTextView = (TextView) findViewById(R.id.totalScore);
        priceTextView.setText("" + totalScore);
    }


}
