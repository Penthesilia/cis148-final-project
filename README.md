# README #

### This is the repository for my CIS148 Final Project - Football Picks ###

* This is a project to create a sheet for my husband, Nick, to track his weekly football picks.  Every week he lists the football games, records the team the line thinks will win and then records his own pick.  He ranks them from 1 to 16 (assuming 16 games) and if his team wins he gets +points.  If his team loses he gets -points.
* Version 1

- This app has the weeks games pre-entered (although ultimately it should link to a database of weekly games if possible so each weeks games are updated automatically)
- There are entry fields for Nick to write in the line's picks and his won
- There is a boolean toggle to mark if the team has won or lost.
- Based on the boolean toggle, his score will be calculated when he hits the submit button.
- It would be nice if the app saved his sheet, but I wasn't able to get that to work yet (I considered using SharedPreferences but that seems inefficient.  I need to explore more)